{
  programs.git = {
    enable = true;
    userName = "Ben Justus Bals";
    userEmail = "benbals@posteo.de";

    signing = {
      key = "F9119EC8FCC56192B5CF53A0BF4F64254BD8C8B5";
      signByDefault = true;
    };

    extraConfig = {
      core = {
        editor ="nvim";
      };
    };
  };
}
