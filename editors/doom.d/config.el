;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Ben J. Bals"
      user-mail-address "benbals@posteo.de")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "IBM Plex Mono" :size 20)
      doom-variable-pitch-font (font-spec :family "IBM Plex Serif" :size 30)
      mixed-pitch-set-height t)

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
(setq doom-theme 'modus-operandi
      modus-operandi-theme-rainbow-headings t
      modus-operandi-theme-variable-pitch-headings t
      modus-operandi-theme-scale-headings t)

;; If you intend to use org, it is recommended you change this!
(setq org-directory "~/Dropbox/")

;; roam
(require 'org-roam-protocol)
(setq org-roam-directory "~/Dropbox")
(setq org-roam-file-exclude-regexp "orgmode-koenig")
(add-hook 'after-init-hook 'org-roam-mode)

(setq org-roam-capture-templates
  '(("d" "default" plain (function org-roam--capture-get-point)
    "%?"
    :file-name "roam/%<%Y%m%d%H%M%S>-${slug}"
    :head "#+title: ${title}\n#+roam_tags: \n#+STARTUP: latexpreview\n"
    :unnarrowed t)
  ("t" "TI1" plain (function org-roam--capture-get-point)
    "%?"
    :file-name "arbeit/ti1/notes/%<%Y%m%d%H%M%S>-${slug}"
    :head "#+INCLUDE: \"base.org\" :minlevel 1\n#+title: ${title}\n#+roam_tags: ti1\n#+STARTUP: latexpreview\n"
    :unnarrowed t)))

(after! org-roam
  (map! :leader
   :prefix "n"
    :desc "org-roam" "l" #'org-roam
    :desc "org-roam-insert" "i" #'org-roam-insert
    :desc "org-roam-switch-to-buffer" "b" #'org-roam-switch-to-buffer
    :desc "org-roam-find-file" "f" #'org-roam-find-file
    :desc "org-roam-show-graph" "g" #'org-roam-show-graph
    :desc "org-roam-insert" "i" #'org-roam-insert
    :desc "org-roam-capture" "c" #'org-roam-capture))

(require 'company-org-roam)
(use-package company-org-roam
  :when (featurep! :completion company)
  :after org-roam
  :config
   (set-company-backend! 'org-mode '(company-org-roam company-yasnippet company-dabbrev)))

;; browse window history
(map! "<C-left>" #'winner-undo
      "<C-right>" #'winner-redo)

;; org-ref
(setq reftex-default-bibliography '("~/Dropbox/bibliography/references.bib"))

(setq bibtex-completion-bibliography
      '("~/Dropbox/bibliography/references.bib"))

;; see org-ref for use of these variables
(setq org-ref-bibliography-notes "~/Dropbox/bibliography/notes.org"
      org-ref-default-bibliography '("~/Dropbox/bibliography/references.bib")
      org-ref-pdf-directory "~/Dropbox/bibliography/bibtex-pdfs/")

(autoload 'ivy-bibtex "ivy-bibtex" "" t)
;; ivy-bibtex requires ivy's `ivy--regex-ignore-order` regex builder, which
;; ignores the order of regexp tokens when searching for matching candidates.
;; Add something like this to your init file:
(setq ivy-re-builders-alist
     '((ivy-bibtex . ivy--regex-ignore-order)
       (t . ivy--regex-plus)))

(setq org-ref-completion-library 'org-ref-ivy-cite)

;; correctly build bibliography on latex export
;; (setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))

(use-package org-ref)

;; export using ConTeXt instead of LaTeX
;; (setq org-latex-compiler "context")
;; (setq org-latex-pdf-process (list "context %f"))
;; (setq org-latex-default-class "context")


;; (with-eval-after-load 'ox-latex
;;   (setq org-latex-default-packages-alist '())
;;   (add-to-list 'org-latex-classes
;;         '("context"
;;            "[PACKAGES]
;; \\setupcolors[state=start]       % otherwise you get greyscale
;; \\definecolor[headingcolor][r=1,b=0.4]

;; % for the document info/catalog (reported by 'pdfinfo', for example)
;; \\setupinteraction[state=start,  % make hyperlinks active, etc.
;;   title={Hello world!},
;;   subtitle={A ConTeXt template},
;;   author={Sanjoy Mahajan},
;;   keyword={template}]
;; \\setuplayout[topspace=0.5in, backspace=1in, header=24pt, footer=36pt,
;;   height=middle, width=middle]
;; \\setupfooter[style=\\it]
;; \\setupfootertexts[\\date\\hfill\\title]
;; \\setuppagenumbering[location={header,right}, style=bold]
;; % Commands to translate LaTeX environment calls
;; % into the appropriate ConTeXt macros.

;; % The \\end of a \\begin,\\end pair is problematic, since \\stoptext
;; % depends on TeX's \\end.  We fix that as follows, using the
;; % \\everystoptext hook.  (Note that some versions of ConTeXt
;; % have already redefined \\end, but this should all work out.

;; \\unprotect
;; \\let\\@@end=\\end
;; \\appendtoks
;;   \\let\\end=\\@@end
;; \\to \\everystoptext
;; \\protect

;; % With that out of the way, the actual trans-
;; % lation macros are straightforward.

;; \\def\\begin#1{%
;;    \\csname start#1\\endcsname}
;;  \\def\\end#1{%
;;    \\csname stop#1\\endcsname}
;; \\usemodule[amsl]
;; \\usemodule[newmat]

;; \\unprotect
;; \\def\\title#1{\\gdef\\@title{#1}}
;; \\def\\author#1{\\gdef\\@author{#1}}
;; \\def\\date#1{\\gdef\\@date{#1}}
;; \\date{\\currentdate}  % Default to today unless specified otherwise.

;; \\def\\maketitle{%
;;   \\startalignment[center]
;;     \\blank[force,2*big]
;;       {\\tfd \\@title}
;;     \\blank[3*medium]
;;       {\\tfa \\@author}
;;     \\blank[2*medium]
;;       {\\tfa \\@date}
;;     \\blank[3*medium]
;;   \\stopalignment}
;; \\protect
;; [EXTRA]
;; "
;;   ("\\section{%s}" . "\\section*{%s}")
;;   ("\\subsection{%s}" . "\\subsection*{%s}")
;;   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
;;   ("\\paragraph{%s}" . "\\paragraph*{%s}")
;;   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

;; suppress warnings in yasnippet needed for tikzcd snipped
(add-to-list 'warning-suppress-types '(yasnippet backquote-change))

;; If you want to change the style of line numbers, change this to `relative' or
;; `nil' to disable it:
(setq display-line-numbers-type 'relative)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
(defun my-setup-indent (n)
  ;; java/c/c++
  (setq c-basic-offset n)
  ;; web development
  (setq coffee-tab-width n) ; coffeescript
  (setq javascript-indent-level n) ; javascript-mode
  (setq js-indent-level n) ; js-mode
  (setq js2-basic-offset n) ; js2-mode, in latest js2-mode, it's alias of js-indent-level
  (setq web-mode-markup-indent-offset n) ; web-mode, html tag in html file
  (setq web-mode-css-indent-offset n) ; web-mode, css in html file
  (setq web-mode-code-indent-offset n) ; web-mode, js code in html file
  (setq css-indent-offset n) ; css-mode
  (setq typescript-indent-level n) ; typescript
  )
(my-setup-indent 2)

;; commands for dictionary and flycheck
(map! :leader "S d" 'ispell-change-dictionary)
(map! :leader "S c" 'flyspell-buffer)

;; load environment based on direnv
(direnv-mode)

"don't display line numbers in text buffers"
(remove-hook! '(text-mode-hook)
              #'display-line-numbers-mode)

;; org latex and babel
(with-eval-after-load 'org
  (setq org-highlight-latex-and-related '(latex))
  (org-babel-do-load-languages
    'org-babel-load-languages
    '((emacs-lisp . t)
      (gnuplot . t))))

;; use multiple fonts in text mode
(use-package mixed-pitch
  :hook
  ;; If you want it in all text modes:
  (text-mode . mixed-pitch-mode)) ;; or org-mode

(after! org
  "Make LaTeX fragments bigger"
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))

  (add-hook 'org-mode-hook
    (lambda ()
      "Center text"
      (olivetti-mode)
      "Automatically toggle latex fragments"
      (org-fragtog-mode)
      (org-num-mode))))

(use-package! sdcv
  :commands sdcv-search sdcv-list-dictionary
  :config
  (map! :map sdcv-mode-map
        :n "q" #'sdcv-return-from-sdcv
        :nv "RET" #'sdcv-search-word-at-point
        :n "a" #'outline-show-all
        :n "h" (cmd! (outline-hide-sublevels 3))
        :n "o" #'sdcv-toggle-entry
        :n "n" #'sdcv-next-entry
        :n "N" (cmd! (sdcv-next-entry t))
        :n "p" #'sdcv-previous-entry
        :n "P" (cmd! (sdcv-previous-entry t))
        :n "b" #'sdcv-search-history-backwards
        :n "f" #'sdcv-search-history-forwards
        :n "/" (cmd! (call-interactively #'sdcv-search))))

;; open external files
(use-package! crux
  :config (map! "C-c o" 'crux-open-with))

;; command to see the kill ring
(map! :leader "y" 'helm-show-kill-ring)

;; completion
(after! company
  (setq company-idle-delay 0.2
        company-minimum-prefix-length 2)
  (setq company-show-numbers t)

  (add-hook 'evil-normal-state-entry-hook #'company-abort))

(setq-default history-length 1000)
(setq-default prescient-history-length 1000)

; (use-package! lsp-python-ms
;  :ensure t
;  :hook (python-mode . (lambda ()
;                         (require 'lsp-python-ms)
;                         (lsp)))
;  :init ((setq lsp-python-ms-executable (executable-find "python-language-server"))
;         (setq lsp-pyls-plugins-flake8-max-line-length 100)
;(setq lsp-pyls-plugins-flake8-max-line-length 100)
;         ))

(after! notmuch

(setq notmuch-saved-searches
  '((:name "inbox" :query "tag:inbox" :key "i" :sort-order newest-first)
  (:name "unread" :query "tag:unread" :key "u" :sort-order newest-first)
  (:name "accounts" :query "tag:accounts" :key "c" :sort-order newest-first)
  (:name "archive" :query "tag:archive" :key "r" :sort-order newest-first)
  (:name "arbeit" :query "from:@ecs-gmbh.de" :key "w" :sort-order newest-first)
  (:name "hpi" :query "to:ben.bals@student.hpi.de" :key "h" :sort-order newest-first)
  (:name "flagged" :query "tag:flagged" :key "f" :sort-order newest-first)
  (:name "all" :query "*" :key "a" :sort-order newest-first)))

(define-key notmuch-show-mode-map "F"
  (lambda ()
    "mark message as important"
    (interactive)
    (notmuch-show-tag (list "+flagged" "-inbox"))))

(define-key notmuch-show-mode-map "a"
  (lambda ()
    "toggle archive tag for message"
    (interactive)
    (if (member "archive" (notmuch-show-get-tags))
        (notmuch-show-tag (list "-archive +inbox"))
      (notmuch-show-tag (list "+archive" "-inbox"))))))
