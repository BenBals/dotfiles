{
  allowUnfree = true;
  allowBroken = true;

  packageOverrides = pkgs: {
    nix = pkgs.nix.overrideAttrs(oldAttrs: {
      prePatch = ''
        substituteInPlace src/libstore/local-store.cc \
          --replace '(eaName == "security.selinux")' \
                    '(eaName == "security.selinux" || eaName == "system.nfs4_acl")'
      '';
      patches = (oldAttrs.patches or []) ++ [ ./nfs.patch ];
    });
  };
}
