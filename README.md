# Moved to [BenBals/nixos-config](https://gitlab.com/BenBals/nixos-config)

# Home Manager
The files in this folder are managed by [home-manager](https://github.com/rycee/home-manager).

## Installation
1. Add the nix channel for home-manager
```shell
$ nix-channel --add https://github.com/rycee/home-manager/archive/release-18.09.tar.gz home-manager
$ nix-channel --update
```
2. Log out and back in.
3. Do the actual installation
```shell
nix-shell '<home-manager>' -A install
```
4. Symlink the config folder

```shell
ln -sf ~/dotfiles ~/.config/nixpkgs
```

## Configuration not covered by this repo
1. Installing Emacs
```shell

$ wget -O ~/.emacs https://raw.githubusercontent.com/plexus/chemacs/master/.emacs
$ git clone https://github.com/syl20bnr/spacemacs ~/.spacemacs
$ git clone https://github.com/cmiles74/spacemacs-notmuch-layer.git ~/.spacemacs/private/notmuch
$ git clone https://github.com/hlissner/doom-emacs ~/doom-emacs
```
