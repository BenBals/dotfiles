{
  home.sessionVariables.XDG_DATA_DIRS = "$XDG_DATA_DIRS:~/.local/share/applications/";

  home.file."emacsclient.desktop" = {
    target = ".local/share/applications/emacsclient.desktop";
    text = ''
      [Desktop Entry]
      Name=Emacs (Client)
      GenericName=Text Editor
      Comment=Edit text
      MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
      Exec=emacsclient -a "emacs" %F
      Icon=emacs
      Type=Application
      Terminal=false
      Categories=Development;TextEditor;Utility;
      StartupWMClass=Emacs
    '';
  };

  home.file."org-protocol.desktop" = {
    target = ".local/share/applications/org-protocol.desktop";
    text = ''
      [Desktop Entry]
      Name=Org-Protocol
      Exec=emacsclient %u
      Icon=emacs-icon
      Type=Application
      Terminal=false
      MimeType=x-scheme-handler/org-protocol
    '';
  };
}
