{ pkgs, ... }:

{
  xdg.mimeApps = {
    enable = true;
    associations.added = {
      "application/pdf" = [ "okularApplication_fax.desktop" "emacs.desktop" "thunderbird.desktop" ];
      "application/octet-stream" = [ "okularApplication_pdf.desktop" ];
      "text/plain" = [ "emacsclient.desktop" "writer.desktop" "nvim.desktop" ];
      "text/calendar" = [ "thunderbird.desktop" ];
      "application/vnd.ms-powerpoint" = [ "impress.desktop" "okularApplication_plucker.desktop" ];
    };
    defaultApplications = {
      "application/pdf"= [ "okularApplication_fax.desktop" ];
      "x-scheme-handler/sgnl"= [ "signal-desktop.desktop" ];
      "text/html" = [ "firefox.desktop" ];
      "x-scheme-handler/http" = [ "firefox.desktop" ];
      "x-scheme-handler/https" = [ "firefox.desktop" ];
      "x-scheme-handler/about" = [ "firefox.desktop" ];
      "x-scheme-handler/unknown" = [ "firefox.desktop" ];
      "x-scheme-handler/org-protocol" = [ "org-protocol.desktop" ];
    };
  };
}

