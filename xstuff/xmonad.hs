import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Util.Run (spawnPipe, safeSpawn)
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Actions.CycleWS as CycleWS
import qualified XMonad.StackSet as W
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders
import XMonad.Util.NamedWindows (getName)
import XMonad.Config.Kde
import XMonad.Config.Desktop

import Graphics.X11.ExtraTypes.XF86

import qualified DBus as D
import qualified DBus.Client as D
import qualified Codec.Binary.UTF8.String as UTF8

import System.IO
import System.Environment
import System.FilePath

import Control.Monad
import Data.List
import qualified Data.Map
import Data.Maybe (fromMaybe)
import Data.Function (on)

import NamedActions

modm = mod4Mask

main = do
  home <- getEnv "HOME"
  path <- fromMaybe "" <$> lookupEnv "PATH"
  setEnv "PATH" (path ++ ":" ++ (home </> "dotfiles/bin"))
  (xmonad) =<< (cfg $ baseConfig)

cfg config = do
    return $ addKeys myKeys config
  where
    addKeys keys c = addDescrKeys' ((modm, xK_F1), xMessage) (const keys) c

baseConfig = kde4Config
    { manageHook = manageDocks <+> manageHook kdeConfig <+> myManageHook <+>  manageHook def
    , layoutHook
      = avoidStruts
      -- $ noBorders
      $ spacingRaw True (Border 0 0 0 0) False (Border 5 5 5 5) True
      $ layoutHook def
    , modMask = mod4Mask -- Rebind Mod to Super
    , startupHook = startup
    , terminal = myTerminal
    , workspaces = myWorkspaces
    , normalBorderColor  = myNormalBorderColor
    , focusedBorderColor = myFocusedBorderColor
    }

startup = do
  spawn "feh --bg-fill ~/dotfiles/xstuff/background.jpg"
  spawnOnce "nm-applet"
  spawnOnce "~/dotfiles/bin/setup"

myKeys :: [((KeyMask, KeySym), NamedAction)]
myKeys =
  [ ((modm .|. shiftMask, xK_Return), addName "start terminal" $ spawn myTerminal)
  , ((modm, xK_Return), addName "start emacsclient" $ spawn "emacsclient --create-frame")

  , ((modm, xK_h), addName "next window" $ windows W.focusDown)
  , ((modm, xK_s), addName "prev window" $ windows W.focusUp)
  , ((modm, xK_e), addName "expand master pane" $ sendMessage Expand)
  , ((modm, xK_o), addName "shrink master pane" $ sendMessage Shrink)
  , ((modm .|. shiftMask, xK_h), addName "move window forward" $ windows W.swapDown)
  , ((modm .|. shiftMask, xK_s), addName "move window backwards" $ windows W.swapUp)


  , ((modm, xK_n), addName "next workspace" $ CycleWS.nextWS)
  , ((modm, xK_t), addName "prev workspace" $ CycleWS.prevWS)
  , ((modm, xK_Tab), addName "toggle workspace" $ CycleWS.toggleWS)

  , ((modm, xK_i), addName "tiling" $ withFocused $ windows . W.sink)
  , ((modm .|. shiftMask, xK_i), addName "inc n of windows in master" $ sendMessage (IncMasterN 1))
  , ((modm .|. shiftMask, xK_d), addName "dec n of windows in master" $ sendMessage (IncMasterN (-1)))

--  , ((0, xF86XK_MonBrightnessUp  ), addName "increase brightness" $ changeBacklight 10)
  , ((0, xF86XK_MonBrightnessUp  ), addName "increase brightness" $ spawn "~/dotfiles/bin/brightness +0.1")
  , ((0, xF86XK_MonBrightnessDown), addName "decrease brightness" $ spawn "~/dotfiles/bin/brightness -0.1")
  , ((0, xF86XK_AudioRaiseVolume), addName "raise volume" $ volChange 10)
  , ((0, xF86XK_AudioLowerVolume), addName "lower volume" $ volChange (-10))
  , ((0, xF86XK_AudioMute), addName "toggle mute" toggleMuted)
  , ((0, xF86XK_AudioMicMute), addName "toggle mic" $ toggleMicMute)
  , ((modm .|. controlMask, xK_m), addName "toggle mic" $ toggleMicMute)
  , ((0, xF86XK_Display), addName "update monitor" $ spawn "autorandr horizontal")

  , ((modm, xK_space), addName "search program" $ spawn "LANG=en_IE.UTF-8 rofi -show run")
  , ((modm .|. shiftMask, xK_space), addName "krunner" $ spawn "krunner \"\"")
  , ((modm, xK_m), addName "monitor && keyboard" $ spawn "~/dotfiles/bin/monitor")
  , ((modm .|. shiftMask, xK_m), addName "2 displays" $ spawn "autorandr common")
  , ((modm, xK_d), addName "screenshot" $ spawn "scrot ~/Dropbox/screenshots/$(date -Iseconds).png")
  , ((modm, xK_c), addName "cycle layouts" $ sendMessage NextLayout)
  , ((modm .|. shiftMask, xK_semicolon), addName "kill" $ kill)
  , ((modm, xK_r), addName "restart xmonad" $ spawn "xmonad --restart")
  ] ++ workspaceKeys ++
    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), addName "focus screen" $ screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_period, xK_comma, xK_p] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

myMouseBindings (XConfig {XMonad.modMask = modm}) = Data.Map.fromList $
  -- mod-button1, Set the window to floating mode and move by dragging
  [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                      >> windows W.shiftMaster))
  -- mod-button2, Raise the window to the top of the stack
  , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
  -- mod-button3, Set the window to floating mode and resize by dragging
  , ((modm, button3), (\w -> focus w >> mouseResizeWindow w))
  ]


myWorkspaces = ["E", "2", "3", "4", "5"]

workspaceKeys :: [((KeyMask, KeySym), NamedAction)]
workspaceKeys =
  [ ((modm, xK_parenleft), addName "Go to WS E" $ windows $ W.view (myWorkspaces !! 0))
  , ((modm, xK_parenright), addName "Go to WS 2" $ windows $ W.view $ myWorkspaces !! 1)
  , ((modm, xK_braceright), addName "Go to WS 3" $ windows $ W.view $ myWorkspaces !! 2)
  , ((modm, xK_plus), addName "Go to WS 4" $ windows $ W.view $ myWorkspaces !! 3)
  , ((modm, xK_braceleft), addName "Go to WS 5" $ windows $ W.view $ myWorkspaces !! 4)
  , ((modm .|. shiftMask, xK_parenleft), addName "Move to WS E" $ windows $ W.shift $ myWorkspaces !! 0)
  , ((modm .|. shiftMask, xK_parenright), addName "Move to WS 2" $ windows $ W.shift $ myWorkspaces !! 1)
  , ((modm .|. shiftMask, xK_braceright), addName "Move to WS 3" $ windows $ W.shift $ myWorkspaces !! 2)
  , ((modm .|. shiftMask, xK_plus), addName "Move to WS 4" $ windows $ W.shift $ myWorkspaces !! 3)
  , ((modm .|. shiftMask, xK_braceleft), addName "Move to WS 5" $ windows $ W.shift $ myWorkspaces !! 4)
 ]

myTerminal = "alacritty"

myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#6600ff"

-- ACTIONS
toggleMuted :: MonadIO m => m ()
toggleMuted = spawn "pamixer --toggle-mute"

volChange :: MonadIO m => Double -> m ()
volChange by = do
  when (by > 0) $ spawn ("pamixer --increase " ++ show (floor by))
  when (by < 0) $ spawn ("pamixer --decrease " ++ show (floor (-by)))

toggleMicMute :: MonadIO m => m ()
toggleMicMute = do
  spawn "pamixer --list-sources | awk '(NR>1) { print $2 }' | xargs -i{} pamixer --source {} -t"

changeBacklight :: MonadIO m => Double -> m ()
changeBacklight by = do
  when (by > 0) $ spawn ("backlight +" ++ show (floor by))
  when (by < 0) $ spawn ("backlight -" ++ show (-floor by))


-- Polybar interop
fg        = "#ebdbb2"
bg        = "#282828"
gray      = "#a89984"
bg1       = "#3c3836"
bg2       = "#504945"
bg3       = "#665c54"
bg4       = "#7c6f64"

green     = "#b8bb26"
darkgreen = "#98971a"
red       = "#fb4934"
darkred   = "#cc241d"
yellow    = "#fabd2f"
blue      = "#83a598"
purple    = "#d3869b"
aqua = "#8ec07c"

myManageHook = composeAll  
  [ className =? "yakuake" --> doFloat  
  , className =? "Yakuake" --> doFloat  
  , className =? "Kmix" --> doFloat  
  , className =? "kmix" --> doFloat  
  , className =? "plasma" --> doFloat  
  , className =? "Plasma" --> doFloat  
  , className =? "plasma-desktop" --> doFloat  
  , className =? "Plasma-desktop" --> doFloat  
  , className =? "plasmashell" --> doFloat  
  , className =? "krunner" --> doFloat  
  , className =? "ksplashsimple" --> doFloat  
  , className =? "ksplashqml" --> doFloat  
  , className =? "ksplashx" --> doFloat
  , className =? "Gimp" --> doFloat
  ]  
