{
  programs.fish = {
    enable = true;
    shellInit = ''
      set PATH ~/dotfiles/bin $PATH
      set PATH /usr/bin $PATH
      direnv hook fish | source
      alias c "cd (sk)"
      alias l "ls -lha"
      alias file-watch "rg -l . | entr -s"
      alias nix-build-watch "rg -l . | entr -rs 'nix-build'"
      alias copy "xclip -selection c"
      alias s "sk --ansi -i -c 'rg --color=always --line-number \"{}\"'"
      export _JAVA_AWT_WM_NONREPARENTING=1
      export GPG_TTY=(tty)
      export SSH_AUTH_SOCK=(gpgconf --list-dirs agent-ssh-socket)
      zoxide init fish | source
      export LANG="en_IE.UTF-8"
    '';
  };


  xdg.configFile."fish/functions/kvm.fish".source = ./terminal/kvm.fish;
}
